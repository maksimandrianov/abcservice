$(document).ready(function() {
    document.abcService54 = (function() {

        function init () {
            var modal = $("#abcModal");
            modal.on("shown.bs.modal", function(e) {
                if (!e.hasOwnProperty("relatedTarget"))
                    return;

                var name = $(e.relatedTarget).find(".page2__item__title").text().trim();
                $("#abcModal input[name='reason']").val(name);
            });


            waitFor(".page4__map > ymaps", function(map) {
                var width = $(window).width();
                map.css("width", width);
                $(window).resize(function() {
                    width = $(window).width();
                    map.css("width", width);
                });
            });

        }

        function makeApplication(id) {
            $(id).submit(function(e) {
                e.preventDefault();

                var form = $(this);
                var url = form.attr("action");
                $.ajax({
                    type: "POST",
                    url: url,
                    data: form.serialize(),
                    success: function(data) {
                        abcAlert.init();
                        if (data.hasOwnProperty("msg"))
                            abcAlert.setMsg(data["msg"]);

                        var error = data.hasOwnProperty("error") ? data["error"] : 0;
                        if (error === 0)
                            abcAlert.setSuccess();
                        else
                            abcAlert.setError();

                        abcAlert.show();

                        $('#abcModal').modal('hide');
                    }
                });
            });
        }


        var waitFor = function(selector, callback) {
            var s = $(selector);
            if (s.length) {
                callback(s);
            } else {
                setTimeout(function() {
                    waitFor(selector, callback);
                }, 100);
            }
        };


        var abcAlert = (function() {
            var kErrorClass = "alert-danger";
            var kSuccessClass = "alert-success";
            var kShowClass = "show";
            var kHideClass = "hide";
            var kTimeoutMs = 3000;
            var alert = $("#abcAlertId");

            return {
                init: function() {
                    this.setMsg("");
                    this.setSuccess();
                },
                setMsg: function(msg) {
                    var msgSpan = alert.find("#abcAlertMsgId");
                    msgSpan.text(msg);
                },
                setError: function() {
                    alert.switchClass(kSuccessClass, kErrorClass);
                },
                setSuccess: function() {
                    alert.switchClass(kErrorClass, kSuccessClass);
                },
                show: function() {
                    alert.switchClass(kHideClass, kShowClass);
                    var self = this;
                    setTimeout(function() {
                        if (alert.hasClass(kHideClass))
                            return;

                        self.hide()
                    }, kTimeoutMs);

                },
                hide: function() {
                    alert.switchClass(kShowClass, kHideClass);
                }
            }
        })();


        return {
            init: init,
            abcAlert: abcAlert,
            makeApplication: makeApplication,
        };
    })();

    document.abcService54.init();
});