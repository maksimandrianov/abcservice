from datetime import datetime
from functools import wraps
from threading import Thread

from flask import request, Flask, make_response, render_template, jsonify
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_mail import Mail, Message

app = Flask(__name__, static_url_path="/static")
app.config.from_pyfile("config.cfg")

limiter = Limiter(
    app,
    key_func=get_remote_address,
    default_limits=[]
)

mail = Mail(app)


ENV = {
    "title": "Ремонт бытовой техники в Новосибирске",
    "menu": [
        {"name": "Услуги", "url": "#urlPage2"},
        {"name": "Цены", "url": ""},
        {"name": "Схема работы", "url": ""},
        {"name": "География доставки", "url": "#urlPage4"},
        {"name": "Команда", "url": ""},
        {"name": "Отзывы", "url": "#urlPage6"},
        {"name": "Контакты", "url": "#idFooter"},
    ],
    "page2": {
        "items": [
            {"title": "Плиты и духовые шкафы", "img": "kitchen-stove.svg"},
            {"title": "Стиральные машины", "img": "washer.svg"},
            {"title": "Варочные панели", "img": "hob.svg"},
            {"title": "Холодильники и морозильные камеры", "img": "fridge.svg"},
            {"title": "Посудомоечные машины", "img": "dishwasher.svg"},
            {"title": "Водонагреватели", "img": "water-heater.svg"},
            {"title": "СВЧ-печи", "img": "microwave.svg"},
            {"title": "Мясорубки", "img": "meat-grinder.svg"},
            {"title": "Кофемашины", "img": "coffee-machine.svg"},
        ],
    },
}


@app.errorhandler(429)
def ratelimit_handler(e):
    return make_response(jsonify({
        "error": 1, "msg": "Превышено количество запросов, повторите позже."
    }))


def in_another_thread(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        thr = Thread(target=f, args=args, kwargs=kwargs)
        thr.start()
    return wrapper


@in_another_thread
def send_async_email(app, msg):
    with app.app_context():
        mail.send(msg)


@app.route("/application-modal", methods=["POST"])
@app.route("/application", methods=["POST"])
@limiter.limit("6 per minute")
def application():
    if request.method == 'POST':
        print(request.form)
        name = request.form.get("name", "")
        phone = request.form.get("phone", "")
        reason = request.form.get("reason", "")
        dt = request.form.get("datetime", "")

        if not name or not (3 <= len(name) <= 64):
            return jsonify({"error": 1, "msg": "Имя невалидно!"})

        if not phone or not (7 <= len(phone) <= 64):
            return jsonify({"error": 1, "msg": "Телефонный номер невалиден!"})

        if reason and not (8 <= len(reason) <= 512):
            return jsonify({"error": 1, "msg": "Причина невалидена!"})

        if dt and not (8 <= len(dt) <= 64):
            return jsonify({"error": 1, "msg": "Дата невалидена!"})

        header = f"Новая заявка от {name}[{datetime.now().strftime('%Y-%m-%d %H:%M')}]."
        msg = f"Имя: {name}.\nТелефон: {phone}.\n"
        if reason:
            msg += f"Причина: {reason}.\n"
        if dt:
            msg += f"Удобно обсудить поломку: {dt}\n"

        send_async_email(app, Message(subject=header, body=msg, recipients=app.config["MAILS"].split(";")))
        return jsonify({"error": 0, "msg": "Ваша заявка успешно отправлена! Скоро наши специалисты вам перезвонят."})


@app.route('/')
def index():
    # send_message()
    return render_template("index.html", env=ENV)
