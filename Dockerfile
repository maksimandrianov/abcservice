FROM python:3.6

RUN apt update && apt install -y nginx

COPY . /abcservice54
WORKDIR /abcservice54

RUN pip install -r requirements.txt
RUN cp abcservice54 /etc/nginx/sites-available/default && \
	ln -sf /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default

EXPOSE 80

CMD service nginx restart && \
	 uwsgi --ini abcservice54.ini --logto abcservice54.log
